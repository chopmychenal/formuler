from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name = 'searcher'),
    path("theory/", views.theory, name = 'theory'),
    path("practice/", views.practice, name = 'practice'),
    path("tests/", views.tests, name = 'tests'),
    path("testsK/", views.testsK, name = 'testsK'),
    path("testsD/", views.testsD, name = 'testsD'),
]

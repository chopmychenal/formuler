from django.shortcuts import render
from django.template.response import TemplateResponse

def index(request):
    return  render(request, 'searcher/HTML/Title.html')

def theory(request):
    return  render(request, 'searcher/HTML/Learning.html')

def practice(request):
    return  render(request, 'searcher/HTML/ChooseTest.html')

def tests(request):
    return  render(request, 'searcher/HTML/Test.html')

def testsK(request):
    return  render(request, 'searcher/HTML/TestK.html')

def testsD(request):
    return  render(request, 'searcher/HTML/TestD.html')